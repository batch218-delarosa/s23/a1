// console.log("Testing");

let trainer = {
	name: 'Ask Ketchum',
	age: 10,
	pokemon: ['Pikachu','Charizard','Squirtle','Bulbasaur'],
	friends: {
		Misty: ['Togepi', "Starmie"],
		Brok: ['Geodude','Onyx']
	},
	talk: function() {
		console.log('Pkiachu! I choose you!');
	}

}

console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log(trainer.pokemon);
trainer.talk();


function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;
	this.canFight = true;

	this.tackle = function (target){

		if (!this.canFight) {
			console.log(`${this.name} can't fight anymore because its health is ${this.health}`);
			return;
		}

		console.log(`${this.name} tackled ${target.name}`);

		target.health -= this.attack;

		console.log(`${target.name}'s health is now reduced to ${target.health}`)

		if (target.health <= 0) {
			target.faint();
		}

	}

	this.faint = function () {
		this.canFight = false;
		console.log(`${this.name} has fainted`);
	}
}


let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);


geodude.tackle(pikachu);
mewtwo.tackle(geodude);
geodude.tackle(mewtwo);



